module.exports = {
  env: {
    jest: true,
  },
  extends: ['@zip/react'],
  globals: {
    renderer: 'readonly',
    withTheme: 'readonly',
  },
  overrides: [
    {
      files: ['**/*.ts', '**/*.tsx'],
      rules: {
        // TypeScript takes care of this
        'react/prop-types': 'off',
      },
    },
  ],
  rules: {
    '@typescript-eslint/interface-name-prefix': 'off',
    'import/no-extraneous-dependencies': [
      'error',
      {
        // Allow React and Storybook as devDependencies since we don't want to ship them
        devDependencies: true,
      },
    ],
    // Styled components rely a lot on third party components and props
    'react/jsx-props-no-spreading': 'off',
  },
};
