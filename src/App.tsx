/* eslint-disable react/react-in-jsx-scope */
// import React from 'react';
import { StyledEngineProvider } from '@mui/material';
import { FunctionComponent } from 'react';
import { VGSCollect } from './vgsCollect';

const App: FunctionComponent = () => {
  return (
    <>
      <StyledEngineProvider injectFirst>
        <VGSCollect
          vaultId="tntq4dwvhri"
          accountId={123}
          product={1}
          consumerId="abc"
          originatorEmail="abc@xyz.com"
        />
      </StyledEngineProvider>
    </>
  );
};

export default App;
