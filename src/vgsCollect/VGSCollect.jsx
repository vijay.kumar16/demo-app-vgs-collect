import React, { useCallback, useEffect, useState } from 'react';
import Checkbox from '@mui/material/Checkbox';
import Button from '@mui/material/Button';
import { loadVGSCollect } from '@vgs/collect-js';
import './style.css';

const label = { inputProps: { 'aria-label': 'Checkbox demo' } };

const css = {
  fontFamily: '"Sharp Grotesk Book" !important',
  boxSizing: 'border-box',
  lineHeight: '1.5em',
  fontSize: '14px',
  fontWeight: '200',
  border: '2px solid',
  borderRadius: '4px',
  color: 'rgb(156 158 163)',
  width: '100%',
  height: '100%',
  paddingLeft: '2rem',
  '&.invalid.touched': {
    color: 'red',
  },
  '&.valid': {
    color: 'green',
  },
  // '&:focus': {
  //   borderColor: '#6542BE',
  // },
  '&::placeholder': {
    color: 'rgb(156 158 163)',
    transition: 'all 0.3s ease-in-out',
    visibility: 'visible !important',
  },
  ':focus::-webkit-input-placeholder': {
    color: ' #000000',
    fontSize: '14px',
    background: 'white',
    display: 'block',
    width: '84px',
    transform: 'translateY(-15px)',
    visibility: 'hidden !important',
  },
};

const expiryCss = { ...css };
expiryCss.width = '82%';

const VGSCollect = ({
  vaultId,
  environment,
  version,
  accountId,
  product,
  consumerId,
  originatorEmail,
}) => {
  const [form, setForm] = useState({});
  const [formDetails, setformDetails] = useState({
    card_number: null,
    card_holder: { isFocused: false, isEmpty: true },
    card_exp: { isFocused: false, isEmpty: true },
    card_cvc: { isFocused: false, isEmpty: true },
  });
  const [isDefault, setIsDefault] = useState(true);
  const [response, setResponse] = useState('');

  const initalizeForm = useCallback((vgsCollect) => {
    const vgsForm = vgsCollect.init((state) => {
      console.log(state);
      updateFormData(state);
    });

    vgsForm.field('#card-name', {
      type: 'text',
      name: 'card_holder',
      placeholder: 'Card holder name*',
      validations: ['required'],
      autoComplete: 'cc-name',
      css,
    });
    vgsForm.field('#card-number', {
      type: 'card-number',
      name: 'card_number',
      successColor: '#4F8A10',
      errorColor: '#D8000C',
      placeholder: 'Card number*',
      showCardIcon: true,
      validations: ['required', 'validCardNumber'],
      autoComplete: 'cc-number',
      css,
    });
    vgsForm.field('#card-cvc', {
      type: 'card-security-code',
      name: 'card_cvc',
      successColor: '#4F8A10',
      errorColor: '#D8000C',
      placeholder: 'CVV*',
      maxLength: 3,
      validations: ['required', 'validCardSecurityCode'],
      css: expiryCss,
    });
    vgsForm.field('#card-expiry', {
      type: 'card-expiration-date',
      name: 'card_exp',
      successColor: '#4F8A10',
      errorColor: '#D8000C',
      placeholder: 'Expiry*',
      validations: ['required', 'validCardExpirationDate'],
      autoComplete: 'cc-exp',
      css: expiryCss,
    });
    setForm(vgsForm);
  }, []);

  const loadForm = useCallback(async () => {
    const vgsCollect = await loadVGSCollect({
      vaultId,
      environment,
      version,
    }).catch((e) => {
      console.log(e);
    });
    initalizeForm(vgsCollect);
  }, [environment, initalizeForm, vaultId, version]);

  useEffect(() => {
    loadForm();
  }, [loadForm]);

  const updateFormData = (state) => {
    const cardType = ['card_holder', 'card_number', 'card_exp', 'card_cvc'];
    cardType.forEach((value) => {
      setformDetails((prevUserData) => {
        return {
          ...prevUserData,
          [value]: state[value],
        };
      });
    });
  };

  const handleFormSubmit = (e) => {
    e.preventDefault();
    form.submit(
      '/post',
      {
        data: (formData) => {
          return {
            ...formData,
            isDefault,
            accountId,
            product,
            consumerId,
            originatorEmail,
          };
        },
      },
      (status, data) => {
        setResponse(JSON.stringify(data, null, 4));
      },
    );
  };
  return (
    <div className="centered-section">
      <div>
        <div className="card-main-container">
          <div>
            <div className="main-title">Link a debit card</div>
            <div className="sub-title">
              Store securely for future repayments.
            </div>
          </div>
        </div>
        <form id="collect-form">
          <div className="group">
            <div className="card-field">
              <label>
                {formDetails &&
                  (formDetails.card_holder.isFocused ||
                    !formDetails.card_holder.isEmpty) && (
                    <span
                      className={`card-holder-name${
                        formDetails.card_holder.isFocused
                          ? ' focussed-field'
                          : ''
                      }`}
                    >
                      Card holder name
                    </span>
                  )}
                <div id="card-name" className="field" />
              </label>
            </div>
            <div className="card-field">
              <label>
                {formDetails &&
                  formDetails.card_number &&
                  (formDetails.card_number.isFocused ||
                    !formDetails.card_number.isEmpty) && (
                    <span
                      className={`card-holder${
                        formDetails.card_number.isFocused
                          ? ' focussed-field'
                          : ''
                      }`}
                    >
                      Card number*
                    </span>
                  )}
                <div id="card-number" className="field" />
                {/* {formDetails &&
                formDetails.card_number &&
                (formDetails.card_number.isTouched ||
                  !formDetails.card_number.isEmpty) && (
                  <div>valid holder name is required</div>
                )} */}
              </label>
              {formDetails &&
                formDetails.card_number &&
                !formDetails.card_number.isEmpty &&
                !formDetails.card_number.isValid && (
                  <div className="field-error">
                    Please enter a valid card number
                  </div>
                )}
            </div>
            <div className="card-field flex">
              <div>
                <label>
                  {formDetails &&
                    formDetails.card_exp &&
                    (formDetails.card_exp.isFocused ||
                      !formDetails.card_exp.isEmpty) && (
                      <span
                        className={`card-expiry${
                          formDetails.card_exp.isFocused
                            ? ' focussed-field'
                            : ''
                        }`}
                      >
                        Expiry*
                      </span>
                    )}
                  <div id="card-expiry" className="field" />
                </label>
                {formDetails &&
                  formDetails.card_exp &&
                  !formDetails.card_exp.isEmpty &&
                  !formDetails.card_exp.isValid && (
                    <div className="field-error">Valid date required</div>
                  )}
              </div>
              <div>
                <label>
                  {formDetails &&
                    formDetails.card_cvc &&
                    (formDetails.card_cvc.isFocused ||
                      !formDetails.card_cvc.isEmpty) && (
                      <span
                        className={`card-cvv${
                          formDetails.card_cvc.isFocused
                            ? ' focussed-field'
                            : ''
                        }`}
                      >
                        CVV*
                      </span>
                    )}
                  <div id="card-cvc" className="field" />
                </label>
                {formDetails &&
                  formDetails.card_cvc &&
                  !formDetails.card_cvc.isEmpty &&
                  !formDetails.card_cvc.isValid && (
                    <div className="cvv-field-error">Valid cvv required</div>
                  )}
              </div>
            </div>
            <div className="flex defailt-payment">
              <div>
                <Checkbox
                  {...label}
                  checked={isDefault}
                  onChange={(e, checked) => {
                    setIsDefault(checked);
                  }}
                />
              </div>
              <div className="repayment-text">
                Set as my default repayment method
              </div>
            </div>
            <div className="agreement-container">
              <span className="agreement-text">
                I authorise the direct debit and agree to the terms of the
                &nbsp;
                <span className="agreement-blue">
                  Direct Debit Request and the DIrect Debit Service Agreement.
                </span>
              </span>
            </div>
          </div>
          <div>
            <Button
              color="secondary"
              className="button-bottom"
              onClick={handleFormSubmit}
            >
              Save
            </Button>
            <Button>Cancel</Button>
          </div>

          {/* <button type="submit" onClick={handleFormSubmit}>
          Submit payment
        </button> */}
        </form>
      </div>
      <div className="response-container">
        <pre id="response1">{response}</pre>
      </div>
    </div>
  );
};

VGSCollect.defaultProps = {
  environment: 'sandbox',
  version: '2.16.0',
};

export default VGSCollect;
